package com.example.metrimonylive;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class BaseActivity extends AppCompatActivity {
    Toolbar toolbar;

    public void setUpActionBar(String title,Boolean isBackArrow) {
     init();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBackArrow);
    }
    public void init(){
        Toolbar toolbar = findViewById(R.id.toolbar);

    }
}
