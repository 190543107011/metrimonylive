package com.example.metrimonylive;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity{

  TextView crdlist,crdfavorite,crdsearch;
CardView crdregistration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        listener();


    }
  public  void init(){
        crdregistration = findViewById(R.id.crdregistration);
        crdlist = findViewById(R.id.crdlist);
      crdfavorite = findViewById(R.id.crdfavorite);
      crdsearch = findViewById(R.id.crdsearch);

  }

  public void listener(){

      crdregistration.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent = new Intent(MainActivity.this,registrationactivity.class);
              startActivity(intent);
          }
      });
  }




  }

