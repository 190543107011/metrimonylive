package com.example.metrimonylive.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class Mydatabase extends SQLiteAssetHelper {

    public static final String DATABASE_NAME="metrimony.db";
    public static  final int DATABASE_VERSION = 1;
    public Mydatabase(Context context) {
        super(context, DATABASE_NAME, null,DATABASE_VERSION);
    }
}
