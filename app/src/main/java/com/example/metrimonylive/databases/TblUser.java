package com.example.metrimonylive.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.metrimonylive.model.TblUserModel;

public class TblUser extends Mydatabase {



    public static final String TABLE_NAME = " TblUser";
    public static final String User_Id = "UserId";
    public static final String Name = "Name";
    public static final String Father_Name = "FatherName";
    public static final String Sur_Name = "SurName";
    public static final String Gender = "Gender";
    public static final String Hobbies = "Hobbies";
    public static final String Dob = "Dob";
    public static final String Phone_Number = "PhoneNumber";
    public static final String Language_Id = "LanguageId";
    public static final String City_Id = "CityId";
    public static final String LanguageId = "LanguageId";
    public static final String CityId = "CityId";
    public static final String Age= "Age";
    public static final String Email = "Email";
    public static final String IsFavorite = "IsFavorite";

    public TblUser(Context context) {
        super(context);
    }

    public long insertUserData(TblUserModel tblUserModel){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv= new ContentValues();
            cv.put(Name,tblUserModel.getName());

        cv.put(Father_Name,tblUserModel.getFatherName());
        cv.put(Sur_Name,tblUserModel.getSurName());
        cv.put(Gender,tblUserModel.getGender());
        cv.put(Dob, tblUserModel.getDob());
        cv.put(Phone_Number,tblUserModel.getPhone());
        cv.put(Email,tblUserModel.getEmail());
        cv.put(IsFavorite,tblUserModel.getIsFavorite());



  long inserted = db.insert(TABLE_NAME,null,cv);




        return inserted;

    }
}
