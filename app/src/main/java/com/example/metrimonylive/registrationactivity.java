package com.example.metrimonylive;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.metrimonylive.databases.TblUser;
import com.example.metrimonylive.model.TblUserModel;
import com.example.metrimonylive.util.Const;

public class registrationactivity extends BaseActivity {

    EditText edtname,edtfathername,edtsurname,edtdob,edtemail,edtphone;
    Spinner splanguage,spcity;
    CheckBox chksinging,chkcooking,chkdancing;
    RadioButton rbmale,rbfemale;
    Button btnsubmit;
    TblUserModel tblUserModel;
    TblUser tblUser;
    long insertedcheck;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrationactivity);
        init();
        listener();



    }
    public void init(){
        edtname = findViewById(R.id.edtname);
        edtfathername = findViewById(R.id.edtfathername);
        edtsurname = findViewById(R.id.edtsurname);
        edtdob = findViewById(R.id.edtdob);
        edtemail = findViewById(R.id.edtemail);
        edtphone = findViewById(R.id.edtphone);
        spcity = findViewById(R.id.spcity);
        splanguage = findViewById(R.id.splanguage);
        chkcooking = findViewById(R.id.chkcooking);
        chkdancing = findViewById(R.id.chkdancing);
        chksinging= findViewById(R.id.chksingig);
        rbmale = findViewById(R.id.rbmale);
       rbfemale = findViewById(R.id.rbfemale);
       btnsubmit = findViewById(R.id.btnsubmit);
        tblUserModel = new TblUserModel();
        tblUser = new TblUser(this);

    }

    public void listener(){
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (isvalide()){

                   tblUserModel.setName(edtname.getText().toString());
                   tblUserModel.setFatherName(edtfathername.getText().toString());
                   tblUserModel.setSurName(edtsurname.getText().toString());
                   tblUserModel.setDob(edtdob.getText().toString());
                   tblUserModel.setGender(rbmale.isChecked() ? Const.MALE :Const.FEMALE);
                   tblUserModel.setEmail(edtemail.getText().toString());
                   tblUserModel.setPhone(edtphone.getText().toString());
                   tblUserModel.setIsFavorite(0);

                   long insertedcheck = tblUser.insertUserData(tblUserModel);

                   if (insertedcheck>0){
                       Toast.makeText(registrationactivity.this, "sucessfull", Toast.LENGTH_SHORT).show();
                   }else {
                       Toast.makeText(registrationactivity.this, "Not sucessfull", Toast.LENGTH_SHORT).show();
                   }

               }
            }
        });

    }


        Boolean isvalide(){
            boolean  booleanisvalide = true;


            String email = edtemail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            String MobilePattern = "[0-9]{10}";

            if (edtname.getText().toString().isEmpty()){
                booleanisvalide =false;
                edtname.setError("Enter Name");
            }
            if (edtfathername.getText().toString().isEmpty()){
                booleanisvalide =false;
                edtfathername.setError("Enter Fathername");
            }
            if (edtsurname.getText().toString().isEmpty()){
                booleanisvalide =false;
                edtsurname.setError("Enter Surname");
            }
            if (!(rbmale.isChecked()||rbfemale.isChecked())){
                booleanisvalide =false;
                Toast.makeText(this, "Choose any one", Toast.LENGTH_SHORT).show();
            }

            if (!email.matches(emailPattern)){
                booleanisvalide =false;
                edtemail.setError("Enter Email");
            }
            if (!edtphone.getText().toString().matches(MobilePattern)){
                booleanisvalide =false;
                edtphone.setError("Enter Phonenumber");
            }
            if (!(chksinging.isChecked()||chkdancing.isChecked()||chkcooking.isChecked())){
                booleanisvalide =false;
                Toast.makeText(this, "Choose any one", Toast.LENGTH_SHORT).show();
            }
            return booleanisvalide;



    }

}